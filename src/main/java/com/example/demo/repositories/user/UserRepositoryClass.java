package com.example.demo.repositories.user;

import com.example.demo.entiti.User;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@RequiredArgsConstructor
@Repository
public class UserRepositoryClass implements UserRepository {


    private final JdbcTemplate jdbcTemplate;

    private String REGISTRATION_USER_BY_LOGIN = "INSERT INTO users (user_name,user_password,user_entryData) VALUES (?,?,?)";

    private String FIND_USER_ID_BY_USER_NAME = "SELECT id_users FROM users WHERE user_name=?;\n";

    private String FIND_USER_BY_USER_NAME = "SELECT user_name,user_password,user_entryData" +
            " FROM users WHERE user_name LIKE ?"; // запрос для получение пользователя по логину

    private String CHECK_DATA_EXITE = "SELECT date_exite FROM visit WHERE id_users=?;\n";

    private String CHECK_DATA_ENTRY = "SELECT date_entry FROM visit WHERE id_users=?;\n";

    private String GET_OVERALL_TIME = "SELECT overall_time FROM visit WHERE id_users=?;\n";

    private String DELETE_TABLE_BY_USER_ID = "DELETE FROM visit WHERE id_users=?";

    private String GET_DATA_START_ENTRY = "SELECT date_entrylong FROM visit WHERE id_users=?;\n";

    private String UPDATE_TABLE_VISIT = "UPDATE visit SET overall_time = ?,date_exite=?,date_exitelong=? WHERE id_users = ?;";


    @Override
    public void RepositoryRegistrationUserByUser(User user) {
        String login = user.getLogin();
        String password = user.getPassword();
        String dateRegistraction = user.getEntryData();
        jdbcTemplate.update(REGISTRATION_USER_BY_LOGIN, login, password, dateRegistraction);

    }

    @Override
    public String RepositoryFindUserIdByUserName(User user) {

        List<String> id = jdbcTemplate.query(FIND_USER_ID_BY_USER_NAME,

                new Object[]{user.getLogin()},
                (rs, i) -> {
                    return rs.getString("id_users");
                });

        if(id.size()==0){return "0";}else {return id.get(0);}
    }


    @Override
    public List<User> RepositoryFindUserByUserName(User user) {

        return jdbcTemplate.query(FIND_USER_BY_USER_NAME,
                new Object[]{user.getLogin()},
                (rs, i) -> {
                    return new User()
                            .setLogin(rs.getString("user_name"))
                            .setPassword(rs.getString("user_password"))
                            .setEntryData(rs.getString("user_entryData"));
                });
    }

    @Override
    public int RepositoryGetOverallTime(String idUser) {

        List<Integer> listOverall = null;
        int ID = Integer.parseInt(idUser);
        listOverall = jdbcTemplate.query(GET_OVERALL_TIME,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getInt("overall_time");
                });
        return listOverall.get(0);


    }




    public String RepositoryCheckSessionUserByIdUser(String userID) {

        int ID = Integer.parseInt(userID);


        String statusCheck = null;
        List<String> idSessionsUserExit;
        List<String> idSessionsUserStart;

        idSessionsUserExit = jdbcTemplate.query(CHECK_DATA_EXITE,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getString("date_exite");
                });

        idSessionsUserStart = jdbcTemplate.query(CHECK_DATA_ENTRY,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getString("date_entry");
                });

        if (idSessionsUserStart.size() == 0 && idSessionsUserExit.size() == 0) {
            statusCheck = "Auth-no/Exit-no";
        } else {
            if (idSessionsUserStart.get(0) != null && idSessionsUserExit.get(0) == null) {
                statusCheck = "Auth-ok/Exit-no";
            }
            if (idSessionsUserStart.get(0) != null && idSessionsUserExit.get(0) != null) {
                statusCheck = "Auth-ok/Exit-ok";
            }
        }
        return statusCheck;


    }

    @Override
    public void RepositoryDeleteTableByUserId(String idUser) {
        int ID = Integer.parseInt(idUser);
        jdbcTemplate.update(DELETE_TABLE_BY_USER_ID, ID);

    }

    @Override
    public String RepositoryGetDataStartEntry(String idUser) {

        List<String> listTime;
        int ID = Integer.parseInt(idUser);
        listTime = jdbcTemplate.query(GET_DATA_START_ENTRY,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getString("date_entrylong");
                });
        return listTime.get(0);

    }

    @Override
    public void RepositoryUpdateTableVisit(String idUser, String valueOf, Date dateEnterFinish, int timefinal) {

        String dataExist = dateEnterFinish.toString();
        int ID = Integer.parseInt(idUser);
        jdbcTemplate.update(UPDATE_TABLE_VISIT,
                timefinal, dataExist, valueOf, ID);

    }


}
