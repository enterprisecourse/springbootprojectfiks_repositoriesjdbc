package com.example.demo.repositories.sessionsuser;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Repository
public class SessionRepositoryClass implements SessionsRepository{


    private final JdbcTemplate jdbcTemplate;

    private String CHECK_DATA_EXITE = "SELECT date_exite FROM visit WHERE id_users=?;\n"; // получить дату входа

    private String CHECK_DATA_ENTRY = "SELECT date_entry FROM visit WHERE id_users=?;\n"; //  получить дату выхода

    private String ADD_NOTICE_ENTRY_USER = "INSERT INTO visit" +
            " (date_entry,overall_time,id_subscription,id_users,date_entrylong) VALUES (?,?,?,?,?)"; // вставить в таблицу визит посещение

    private String GET_OVERALL_TIME = "SELECT overall_time FROM visit WHERE id_users=?;\n"; // получить время абонемента пользователя

    private String DELETE_TABLE_BY_USER_ID = "DELETE FROM visit WHERE id_users=?";// удалить таблицу

    private String GET_DATA_START_ENTRY = "SELECT date_entrylong FROM visit WHERE id_users=?;\n"; //  получить время для работы

    private String UPDATE_TABLE_VISIT =
            "UPDATE visit SET overall_time = ?,date_exite=?,date_exitelong=? WHERE id_users = ?;"; // аптейт таблицы посещение


    @Override
    public String SessionRepositoryCheckSessionUserByIdUser(String idUser) {

        int ID = Integer.parseInt(idUser);


        String statusCheck = null;
        List<String> idSessionsUserExit;
        List<String> idSessionsUserStart;

        idSessionsUserExit = jdbcTemplate.query(CHECK_DATA_EXITE,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getString("date_exite");
                });

        idSessionsUserStart = jdbcTemplate.query(CHECK_DATA_ENTRY,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getString("date_entry");
                });

        if (idSessionsUserStart.size() == 0 && idSessionsUserExit.size() == 0) {
            statusCheck = "Auth-no/Exit-no";
        } else {
            if (idSessionsUserStart.get(0) != null && idSessionsUserExit.get(0) == null) {
                statusCheck = "Auth-ok/Exit-no";
            }
            if (idSessionsUserStart.get(0) != null && idSessionsUserExit.get(0) != null) {
                statusCheck = "Auth-ok/Exit-ok";
            }
        }
        return statusCheck;

    }

    public void SessionRepositoryAddNoticeEntryUser(Date dateEnterStart, Integer time, String idUserSubscription, String idUser, String dateEnterStartLong) {

        String dataEnry = String.valueOf(dateEnterStart);
        int idUserSub = Integer.parseInt(idUserSubscription);
        int idUs = Integer.parseInt(idUser);
        jdbcTemplate.update(ADD_NOTICE_ENTRY_USER
                , dataEnry, time, idUserSub, idUs, dateEnterStartLong);

    }

    public int SessionRepositoryGetOverallTimeSubscription(String idUser) {
        List<Integer> listOverall = null;
        int ID = Integer.parseInt(idUser);
        listOverall = jdbcTemplate.query(GET_OVERALL_TIME,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getInt("overall_time");
                });
        return listOverall.get(0);
    }

    @Override
    public void SessionRepositoryDeleteTableVisit(String idUser) {

        int ID = Integer.parseInt(idUser);
        jdbcTemplate.update(DELETE_TABLE_BY_USER_ID, ID);

    }

    @Override
    public String RepositoryGetTimeStartEntryApplication(String idUser) {

        List<String> listTime;
        int ID = Integer.parseInt(idUser);
        listTime = jdbcTemplate.query(GET_DATA_START_ENTRY,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getString("date_entrylong");
                });
        return listTime.get(0);

    }

    @Override
    public void RepositoryUpdateTableVisit(String idUser, String valueOf, Date dateEnterFinish, int timefinal) {

        String dataExist = dateEnterFinish.toString();
        int ID = Integer.parseInt(idUser);
        jdbcTemplate.update(UPDATE_TABLE_VISIT,
                timefinal, dataExist, valueOf, ID);

    }


}
