package com.example.demo.repositories.sessionsuser;


public interface MainRepositorySessionsUser<EntityUser, IdUser, EntitySubscription, IdSubscription, Date, Integer, String> {

    String SessionRepositoryCheckSessionUserByIdUser(String idUser);

    void SessionRepositoryAddNoticeEntryUser(Date dateEnterStart, Integer time, String idUserSubscription, String idUser, String dateEnterStartLong);

    public int SessionRepositoryGetOverallTimeSubscription(String idUser);

    void SessionRepositoryDeleteTableVisit(String idUser);

    public java.lang.String RepositoryGetTimeStartEntryApplication(String idUser);

    void RepositoryUpdateTableVisit(java.lang.String idUser, java.lang.String valueOf, java.util.Date dateEnterFinish, int timefinal);

}
