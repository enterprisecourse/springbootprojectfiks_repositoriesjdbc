package com.example.demo.repositories.subscription;

import com.example.demo.entiti.Subscription;
import com.example.demo.entiti.User;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Repository
public class SubscriptionRepositoryClass implements SubscriptionRepository {

    private final JdbcTemplate jdbcTemplate;

    private String FIND_SUBSCRIPTION_BY_USERID = "SELECT subscription_name,subscription_time,subscription_price,subscription_datepurchase" +
            " FROM subscription WHERE id_users=?;\n";

    private String ADD_SUBSCRIPTION = "INSERT INTO subscription" +
            " (subscription_name,subscription_time,subscription_price,subscription_datepurchase,id_users) VALUES"; // вставляем абонемент в таблицу

    private String ADD_SUBSCRIPTION_AND_USER = "INSERT INTO userssubscription" +
            " (userssubscription_purchase_date,userssubscription_state,id_users,id_subscription)\n" +
            "VALUES (?,?,(SELECT id_users FROM users WHERE user_name=?)," +
            "(SELECT id_subscription FROM subscription WHERE subscription_datepurchase=?));";    // вставляем пользователя и абонемент

    private String FIND_SUBSCRIPTION_ID_BY_USERID = "SELECT id_subscription FROM userssubscription WHERE id_users=?;\n";

    private String ADD_NOTICE_ENTRY_USER = "INSERT INTO visit (date_entry,overall_time,id_subscription,id_users,date_entrylong) VALUES (?,?,?,?,?)";


    @Override
    public List<Subscription> RepositoryFindSubscriptionUserId(String userID) {

        int ID = Integer.parseInt(userID);

        List<Subscription> subscriptionList = jdbcTemplate.query(
                FIND_SUBSCRIPTION_BY_USERID,
                new Object[]{ID},

                (rs, i) -> {
                    return new Subscription()
                            .setNames(rs.getString("subscription_name"))
                            .setTime(rs.getInt("subscription_time"))
                            .setPrice(rs.getString("subscription_price"))
                            .setDatePurchase(rs.getString("subscription_datepurchase"));
                });
        return subscriptionList;

    }

    @Override
    public void RepositoryAddSubscription(Subscription subscription, String IdUser) {

        int IDUser = Integer.parseInt(IdUser);
        jdbcTemplate.update(ADD_SUBSCRIPTION + "\n" +
                "(?,?,?,?,?)", subscription.getNames(), subscription.getTime(), subscription.getPrice(), subscription.getDatePurchase(),IDUser);

    }

    @Override
    public String RepositoryFindSubscriptionIdByUserId(String IdUser) {

        int ID = Integer.parseInt(IdUser);
        List<String> idSub = jdbcTemplate.query(FIND_SUBSCRIPTION_ID_BY_USERID,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getString("id_subscription");
                });
        if(idSub.size()==0){return "0";}else {return idSub.get(0);}
    }

    @Override
    public void RepositoryAddSubscriptionAndUser(Subscription subscription, User user) {


        String log = user.getLogin();

        jdbcTemplate.update(ADD_SUBSCRIPTION_AND_USER, subscription.getDatePurchase(), "active", log, subscription.getDatePurchase());


    }

    @Override
    public void RepositoryAddNoticeEntryUser(Date dateEnterStart, Integer time, String idUserSubscription, String idUser, String dateEnterStartLong) {

        String dataEnry = String.valueOf(dateEnterStart);
        int idUserSub = Integer.parseInt(idUserSubscription);
        int idUs = Integer.parseInt(idUser);
        jdbcTemplate.update(ADD_NOTICE_ENTRY_USER
                , dataEnry, time, idUserSub, idUs, dateEnterStartLong);

    }
}
