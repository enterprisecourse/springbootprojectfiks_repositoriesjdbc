package com.example.demo.repositories.subscription;

import java.util.Date;
import java.util.List;

public interface MainRepositorySubsc<EntityUser , IdUser,EntitySubscription,IdSubscription, Date,Integer,String> {

    public void RepositoryAddSubscription(EntitySubscription entitySubscription, String idSubscription); // добавить абонемент в таблицу subscription

    public String RepositoryFindSubscriptionIdByUserId(String IdUser); // получить iD абонемента по имени пользователя

    public List<EntitySubscription> RepositoryFindSubscriptionUserId(String userID); // найти абонемент по IDUser

    public void RepositoryAddSubscriptionAndUser(EntitySubscription entitySubscription, EntityUser entityUser); // добавить абонемент и пользователя в таблицу usersSubscription

    void RepositoryAddNoticeEntryUser(java.util.Date dateEnterStart, java.lang.Integer time, java.lang.String idUserSubscription, java.lang.String idUser, java.lang.String valueOf);
}
