package com.example.demo.service.subscription;

import com.example.demo.entiti.Subscription;
import com.example.demo.entiti.User;

import java.util.Date;
import java.util.List;

public interface MainServiceSubscription {


    public List<Subscription> ServiceSubscrFindSubscriptionBySubscriptionId(String IdSubscription);// получить абонемент по IdSubscription

    public List<Subscription> ServiceSubscrFindSubscriptionUserId(String userID); // найти абонемент по IDUser

    public void ServiceSubscrAddSubscriptionAndUser(Subscription subscription, User user); // добавить абонемент и пользователя в таблицу usersSubscription

    public String ServiceFindSubscriptionIdByUserId(String IdUser); // получить iD абонемента по имени пользователя


    void ServiceSubscrAddSubscription(Subscription subscription, String idUser);

    void ServiceAddNoticeEntryUser(Date dateEnterStart, Integer time, String idUserSubscription, String idUser, String valueOf);
}
