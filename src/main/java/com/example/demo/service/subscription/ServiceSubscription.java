package com.example.demo.service.subscription;

import com.example.demo.entiti.Subscription;
import com.example.demo.entiti.User;
import com.example.demo.repositories.subscription.SubscriptionRepositoryClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ServiceSubscription implements MainServiceSubscription {

    @Autowired
    SubscriptionRepositoryClass subscriptionRepositoryClass;



    @Override
    public List<Subscription> ServiceSubscrFindSubscriptionBySubscriptionId(String IdSubscription) {
        return null;
    }

    @Override
    public List<Subscription> ServiceSubscrFindSubscriptionUserId(String userID) {

        return subscriptionRepositoryClass.RepositoryFindSubscriptionUserId(userID);
    }

    @Override
    public void ServiceSubscrAddSubscriptionAndUser(Subscription subscription, User user) {

        subscriptionRepositoryClass.RepositoryAddSubscriptionAndUser(subscription,user);

    }



    @Override
    public String ServiceFindSubscriptionIdByUserId(String IdUser) {

        return subscriptionRepositoryClass.RepositoryFindSubscriptionIdByUserId(IdUser);
    }


    @Override
    public void ServiceSubscrAddSubscription(Subscription subscription, String idUser) {
        subscriptionRepositoryClass.RepositoryAddSubscription(subscription,idUser);
    }

    @Override
    public void ServiceAddNoticeEntryUser(Date dateEnterStart, Integer time, String idUserSubscription, String idUser, String valueOf) {
        subscriptionRepositoryClass.RepositoryAddNoticeEntryUser(dateEnterStart,time,idUserSubscription,idUser,valueOf);

    }


}


