package com.example.demo.service.sessionsuser;

import com.example.demo.repositories.sessionsuser.SessionRepositoryClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ServiceSessionUser implements MainServiceSessionUser {

    @Autowired
    SessionRepositoryClass sessionRepositoryClass;


    @Override
    public String ServiceSessionCheckSessionUserByIdUser(String userID) {
        return sessionRepositoryClass.SessionRepositoryCheckSessionUserByIdUser(userID);
    }

    @Override
    public void ServiceSessionAddNoticeEntryUser(Date dateEnterStart, Integer time, String idUserSubscription, String idUser, String valueOf) {

        sessionRepositoryClass.SessionRepositoryAddNoticeEntryUser(dateEnterStart,time,idUserSubscription,idUser,valueOf);
    }

    @Override
    public int ServiceSessionGetOverallTimeUser(String idUser) {
        return sessionRepositoryClass.SessionRepositoryGetOverallTimeSubscription(idUser);

    }

    @Override
    public void ServiceSessionDeleteTable(String idUser) {
        sessionRepositoryClass.SessionRepositoryDeleteTableVisit(idUser);
    }

    @Override
    public String ServiceSessionGetDataStartEntry(String idUser) {
        return sessionRepositoryClass.RepositoryGetTimeStartEntryApplication(idUser);
    }


    @Override
    public boolean ServiceSessionCheckTime(int overallTime, double minutes) {
        if (minutes > overallTime) {
            return false;
        }

        return true;
    }

    @Override
    public void ServiceSessionUpdateTable(String idUser, String valueOf, Date dateEnterFinish, int timefinal) {

        sessionRepositoryClass.RepositoryUpdateTableVisit(idUser, valueOf, dateEnterFinish,timefinal);

    }

}
