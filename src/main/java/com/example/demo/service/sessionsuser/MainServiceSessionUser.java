package com.example.demo.service.sessionsuser;

import com.example.demo.entiti.Subscription;
import com.example.demo.entiti.User;

import java.util.Date;
import java.util.List;

public interface MainServiceSessionUser {


    String ServiceSessionCheckSessionUserByIdUser(String userID);

    void ServiceSessionAddNoticeEntryUser(Date dateEnterStart, Integer time, String idUserSubscription, String idUser, String valueOf);

    public int ServiceSessionGetOverallTimeUser(String idUser);


    void ServiceSessionDeleteTable(String idUser);

    String ServiceSessionGetDataStartEntry(String idUser);

    public boolean ServiceSessionCheckTime(int overallTime, double minutes);

    void ServiceSessionUpdateTable(String idUser, String valueOf, Date dateEnterFinish, int timefinal);
}
