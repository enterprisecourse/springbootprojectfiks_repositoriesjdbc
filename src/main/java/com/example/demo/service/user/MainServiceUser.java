package com.example.demo.service.user;

import com.example.demo.entiti.User;

import java.util.Date;
import java.util.List;

public interface MainServiceUser {


    public int ServiceUserGetOverallTime(String idUser); // получаем полное время клиента из таблицы

    public void ServiceUserRegistrationUserByIdLogin(User user); // регистрация пользователя в таблице users

    public boolean ServiceUserFindUserByUserName(User user) throws Exception; // найти пользователя в таблице users по имени

    public String ServiceUserFindUserIdByUserName(User user) throws Exception; // найти Id пользователя в таблице users по имени


    String ServiceCheckSessionUserByIdUser(String userID);

    int ServiceGetOverallTimeUser(String idUser);

    void ServiceDeleteTable(String idUser);

    String ServiceGetDataStartEntry(String idUser);

    boolean ServiceCheckTime(int overallTime, double minutes);

    void ServiceUpdateTableVisit(String idUser, String valueOf, Date dateEnterFinish, int timefinal);
}
