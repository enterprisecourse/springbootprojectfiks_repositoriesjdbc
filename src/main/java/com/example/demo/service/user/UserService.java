package com.example.demo.service.user;

import com.example.demo.entiti.User;
import com.example.demo.repositories.user.UserRepositoryClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserService implements MainServiceUser {

    @Autowired
    UserRepositoryClass repositoryUser;



    @Override
    public int ServiceUserGetOverallTime(String idUser) {
        return 0;
    }

    @Override
    public void ServiceUserRegistrationUserByIdLogin(User user) {


        repositoryUser.RepositoryRegistrationUserByUser(user);

    }

    @Override
    public boolean ServiceUserFindUserByUserName(User user) throws Exception {

        boolean status = false;
        List<User> userList = repositoryUser.RepositoryFindUserByUserName(user);
        System.out.println("userList " + userList);

        if (userList.size() > 0) {
            status=true;
        }

        if (userList.size() == 0) {
            status=false;
        }

        return status;
    }

    @Override
    public String ServiceUserFindUserIdByUserName(User user) throws Exception {

        String UserID = (String) repositoryUser.RepositoryFindUserIdByUserName(user);
        if(UserID.equals("0")){
            throw new Exception("Пользователь с таким логином Не в базе");
        }

        return (String) repositoryUser.RepositoryFindUserIdByUserName(user);

    }

    @Override
    public String ServiceCheckSessionUserByIdUser(String userID) {

        return repositoryUser.RepositoryCheckSessionUserByIdUser(userID);
    }

    @Override
    public int ServiceGetOverallTimeUser(String idUser) {
        return repositoryUser.RepositoryGetOverallTime(idUser);
    }

    @Override
    public void ServiceDeleteTable(String idUser) {
        repositoryUser.RepositoryDeleteTableByUserId(idUser);
    }

    @Override
    public String ServiceGetDataStartEntry(String idUser) {
        return repositoryUser.RepositoryGetDataStartEntry(idUser);
    }

    @Override
    public boolean ServiceCheckTime(int overallTime, double minutes) {
        if (minutes > overallTime) {
            return false;
        }

        return true;
    }

    @Override
    public void ServiceUpdateTableVisit(String idUser, String valueOf, Date dateEnterFinish, int timefinal) {
        repositoryUser.RepositoryUpdateTableVisit(idUser,valueOf,dateEnterFinish,timefinal);
    }


}
