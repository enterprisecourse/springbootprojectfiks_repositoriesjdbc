package com.example.demo.controller.usercontroller;

import com.example.demo.dto.ResponseDto;
import com.example.demo.entiti.User;

import com.example.demo.service.user.MainServiceUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Slf4j
@RestController
@RequiredArgsConstructor
public class UserController {

    @Autowired
    MainServiceUser serviceUser;  // сервис для работы c пользоватенлями

    @PostMapping("addUser") // API добавить пользователя
    public void LoginАpplicationApp(@RequestBody ResponseDto responseDto) throws Exception {

        User user = new User()
                .setLogin(responseDto.getLogin())
                .setPassword(responseDto.getPassword())
                .setEntryData(new Date().toString()); // добавляем нашего пользователя

        if(serviceUser.ServiceUserFindUserByUserName(user)!=true){
            serviceUser.ServiceUserRegistrationUserByIdLogin(user); // регистрируем нашего пользователя
        }else {
            throw new Exception("пользователь уже есть");
        }
    }


}
