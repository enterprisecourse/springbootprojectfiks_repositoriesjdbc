package com.example.demo.controller.subcriptioncontroller;
import com.example.demo.dto.ResponseDto;
import com.example.demo.entiti.SessionApplication;
import com.example.demo.entiti.Subscription;
import com.example.demo.entiti.User;
import com.example.demo.service.subscription.MainServiceSubscription;
import com.example.demo.service.user.MainServiceUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;


@Slf4j
@RestController
@RequiredArgsConstructor
public class SubscriptionController {


    @Autowired
    private SessionApplication sessionApplication;
    static Date dateEnterStart;
    static Date dateEnterFinish;

    @Autowired
    MainServiceSubscription serviceSubscription;  // сервис для работы c пользоватенлями
    @Autowired
    MainServiceUser serviceUser;  // сервис для работы c пользоватенлями

    public Map<String, Subscription> getSubscriptionMap() {
        Map<String, Subscription> subscriptionMap = new HashMap();
        subscriptionMap.put("1", new Subscription().setNames("Subscription 100 hours").setPrice("100").setTime(100));
        return subscriptionMap;
    } // наши абонементы

    @PostMapping("getSubscription") // API запроса для отображения абонементов
    public Map<String, Subscription> getSubscription(@RequestBody ResponseDto responseDto) throws Exception {

        Map<String, Subscription> subscriptionNEW = new HashMap(); // создали новую мапу для работы c абонементами
        Map<String, Subscription> subscriptionMNew = getSubscriptionMap();  // потянули наши абонементы

        User user = new User()
                .setLogin(responseDto.getLogin())
                .setPassword(responseDto.getPassword())
                .setEntryData(new Date().toString()); // создали нашего пользователя которого нам передали


        String idUser = serviceUser.ServiceUserFindUserIdByUserName(user); // нашли ID нашего пользователя
        List<Subscription> listSubsUser = serviceSubscription.ServiceSubscrFindSubscriptionUserId(idUser); // нашли все абонементы пользователя

        if (listSubsUser.size() > 0) {

            if (listSubsUser.size() == 2) {
                return subscriptionNEW;
            }

            List<Subscription> list = new ArrayList<Subscription>(subscriptionMNew.values());

            for (int i = 0; i < list.size(); i++) {

                System.out.println("---------------------------------------");
                System.out.println("list.get(i) " + list.get(i));

                for (int j = 0; j < listSubsUser.size(); j++) {

                    Subscription subscription = listSubsUser.get(j).setDatePurchase(null);
                    Subscription subscription1 = list.get(i);

                    if (!subscription.getNames().equals(subscription1.getNames())) {
                        subscriptionNEW.put(String.valueOf(j + 1), subscription1);
                    }

                }
            }
            return subscriptionNEW;
        }

        return subscriptionMNew;
    }

    @PostMapping("buySubscription") // API запроса для отображения абонементов
    public void mainApp(@RequestBody ResponseDto responseDto) throws Exception {

        User user = new User()
                .setLogin(responseDto.getLogin())
                .setPassword(responseDto.getPassword())
                .setEntryData(new Date().toString()); // создали нашего пользователя которого нам передали

        serviceUser.ServiceUserFindUserByUserName(user);

        if (serviceUser.ServiceUserFindUserByUserName(user) == true) {

            String IdSubscription = responseDto.getSubcription();  // достали ID абонемента

            Subscription subscription = getSubscriptionMap().get(IdSubscription); // формируем наш абонемент
            subscription.setDatePurchase(new Date().toString()); // добавляем дату покупки абонемента

            String idUser = serviceUser.ServiceUserFindUserIdByUserName(user); // нашли ID нашего пользователя

            List<Subscription> listSubsUser = serviceSubscription.ServiceSubscrFindSubscriptionUserId(idUser); // нашли все абонементы пользователя

            for (Subscription subscription1 : listSubsUser) {
                if (subscription1.getNames().equals(subscription.getNames())) {
                    throw new Exception("такой абонемент есть");
                }
            } // перебираем все абонементы пользователя

            serviceSubscription.ServiceSubscrAddSubscription(subscription, idUser); // вставляем абонемент в таблицу
            serviceSubscription.ServiceSubscrAddSubscriptionAndUser(subscription, user); // вставляем абонемент и пользователя в таблицу

        } else {
            throw new Exception("пользователь не в базе");
        }


    }


}
