package com.example.demo.controller.sessionsuser;
import com.example.demo.dto.ResponseDto;
import com.example.demo.entiti.SessionApplication;
import com.example.demo.entiti.Subscription;
import com.example.demo.entiti.User;
import com.example.demo.service.sessionsuser.MainServiceSessionUser;
import com.example.demo.service.subscription.MainServiceSubscription;
import com.example.demo.service.user.MainServiceUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;


@Slf4j
@RestController
@RequiredArgsConstructor
public class SessionController {

    @Autowired
    private SessionApplication sessionApplication;
    static Date dateEnterStart;
    static Date dateEnterFinish;

    @Autowired
    MainServiceSubscription serviceSubscription;  // сервис для работы c абонементами
    @Autowired
    MainServiceUser serviceUser;  // сервис для работы c пользоватенлями
    @Autowired
    MainServiceSessionUser serviceSessionUser;  // сервис для работы с cессиями входа и выхода



    @PostMapping("entryApp") // API запроса для отображения абонементов
    public void entryApp(@RequestBody ResponseDto responseDto) throws Exception {

        User user = new User()
                .setLogin(responseDto.getLogin())
                .setPassword(responseDto.getPassword())
                .setEntryData(new Date().toString());
               // создали нашего пользователя которого нам передали


        dateEnterStart = sessionApplication.GetTime(); // получили дату входа в приложения

        long dateEnterStartLong = dateEnterStart.getTime();  // преобразовали для работы со временем

        String idUser = serviceUser.ServiceUserFindUserIdByUserName(user); // нашли ID нашего пользователя

        String sessionDistributor = serviceSessionUser.ServiceSessionCheckSessionUserByIdUser(idUser); // проверяем есть ли у пользователя не завершенная ссесия

        if (sessionDistributor.equals("Auth-ok/Exit-no")) {
            log.error("Просим завершить ссесию в приложении");
        } // логика когда у клиента есть вход но нет выхода

        if (sessionDistributor.equals("Auth-no/Exit-no")) {

            String idUserSubscription = serviceSubscription.ServiceFindSubscriptionIdByUserId(idUser);  // нашли ID абонемента
            List<Subscription> listSubsUser = serviceSubscription.ServiceSubscrFindSubscriptionUserId(idUser); // нашли все абонементы пользователя
            Subscription subscription = listSubsUser.get(0);

            serviceSessionUser.ServiceSessionAddNoticeEntryUser(dateEnterStart, subscription.getTime(), idUserSubscription, idUser, String.valueOf(dateEnterStartLong));
        }
        // логика когда у клиента не было авторизаций вообще

        if (sessionDistributor.equals("Auth-ok/Exit-ok")) {

            String idUserSubscription = serviceSubscription.ServiceFindSubscriptionIdByUserId(idUser);  // нашли ID абонемента
            List<Subscription> listSubsUser = serviceSubscription.ServiceSubscrFindSubscriptionUserId(idUser); // нашли все абонементы пользователя
            Subscription subscription = listSubsUser.get(0);

            int overallTime = serviceSessionUser.ServiceSessionGetOverallTimeUser(idUser); // получаем время из таблицы

            serviceSessionUser.ServiceSessionDeleteTable(idUser);

            serviceSessionUser.ServiceSessionAddNoticeEntryUser(dateEnterStart, subscription.getTime(), idUserSubscription, idUser, String.valueOf(dateEnterStartLong));



        } // логика когда у клиента была авторизация ранее


    }


    @PostMapping("exiteApp") // API запроса для отображения абонементов
    public void exiteApp(@RequestBody ResponseDto responseDto) throws Exception {

        User user = new User()
                .setLogin(responseDto.getLogin())
                .setPassword(responseDto.getPassword())
                .setEntryData(new Date().toString());
        // создали нашего пользователя которого нам передали

        dateEnterFinish = sessionApplication.GetTime(); // получили дату выхода из приложения
        String idUser = serviceUser.ServiceUserFindUserIdByUserName(user); // нашли ID нашего пользователя

        String sessionDistributor = serviceSessionUser.ServiceSessionCheckSessionUserByIdUser(idUser); // проверяем есть ли у пользователя не завершенная ссесия

        if (sessionDistributor.equals("Auth-no/Exit-no")) {
            log.error("Просим пройти авторизацию");
        } // когда у клиента не было авторизаций


        if (sessionDistributor.equals("Auth-ok/Exit-no")) {

            long dateEnter = dateEnterFinish.getTime();
            List<Subscription> listSubsUser = serviceSubscription.ServiceSubscrFindSubscriptionUserId(idUser); // нашли все абонементы пользователя
            Subscription subscription = listSubsUser.get(0);

            dateEnterStart = new Date(Long.valueOf(serviceSessionUser.ServiceSessionGetDataStartEntry(idUser))); // получаем время входа в приложения

            double minutes = ((dateEnterFinish.getTime() - dateEnterStart.getTime()) / 1000) / 60;  // получаем сколько времени прошло с момента входа до выхода

            int overallTime = serviceSessionUser.ServiceSessionGetOverallTimeUser(idUser); // получаем время из таблицы



            if (serviceSessionUser.ServiceSessionCheckTime(overallTime, minutes) == true) {

                long dateEnterExist = dateEnterStart.getTime();
                int Timefinal = (int) (overallTime - minutes);

                serviceSessionUser.ServiceSessionUpdateTable(idUser, String.valueOf(dateEnter), dateEnterFinish, Timefinal);

            }


        } // когда у клиента есть авторизация но нет завершения

    }





}
