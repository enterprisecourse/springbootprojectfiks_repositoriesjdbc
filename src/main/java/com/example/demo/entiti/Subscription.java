package com.example.demo.entiti;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Subscription {

    private String names;
    private Integer time;
    private String price;
    private String DatePurchase;

}
