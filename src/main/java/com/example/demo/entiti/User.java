package com.example.demo.entiti;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Properties;

@Data
@Accessors(chain = true)
public class User extends Properties {

    private String Login;
    private String password;
    private String entryData;

}
